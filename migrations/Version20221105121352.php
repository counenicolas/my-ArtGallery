<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221105121352 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE paintings ADD height INT NOT NULL, ADD width INT NOT NULL, ADD created_at DATETIME NOT NULL, DROP size, DROP created');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE paintings ADD size VARCHAR(255) NOT NULL, ADD created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', DROP height, DROP width, DROP created_at');
    }
}
