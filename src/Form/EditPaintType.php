<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Paintings;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditPaintType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class,[
                'label' =>'Titre de la peinture'
            ])
            ->add('image', TextType::class,[
                'label' => 'Votre image'
            ])
            ->add('is_published', ChoiceType::class,[
                'label' => 'Publier la peinture',
                'choices' => ['Oui' => 1, 'Non' => 0]
            ])
            ->add('description', TextareaType::class,[
                'label'=> 'Votre description'
            ])
            ->add('Height', IntegerType::class,[
                'label' => 'Hauteur en cm'
            ])
            ->add('Width', IntegerType::class,[
                'label' => 'Largeur en cm'
            ])
            ->add('CreatedAt', DateType::class,[
                'label' => 'Date de la peinture',
                'data' => new \DateTime(),
                'format' => 'dd MM yyyy'
            ])
            ->add('category', EntityType::class,[
                'label' => 'Sélectionner une catégorie',
                'placeholder' => 'Sélectionnez...',
                'class' => Category::class ,
                'choice_label' => 'name'
            ])
            ->add('submit', SubmitType::class,[
                'label' => 'Mettre à jour la peinture'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Paintings::class,
        ]);
    }
}
