<?php

namespace App\Controller;

use App\Repository\PaintingsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{

    /**
     * @param PaintingsRepository $repository
     * @return Response
     */
    #[Route('/', name: 'home')]
    public function afficherhome(PaintingsRepository $repository): Response
    {
        $paintings = $repository->findAll();


        return $this->render('homepage/home.html.twig',[
            'paintings' => $paintings
        ]);
    }

}
