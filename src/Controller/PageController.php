<?php

namespace App\Controller;




use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Flex\Response;
use Symfony\Component\Routing\Annotation\Route;


class PageController extends AbstractController{

    #[Route('/team', name: 'team')]
    public function team()
    {

        return $this->render('page/team.html.twig');
    }

    #[Route('/about', name: 'about')]
    public function about()
    {
        return $this->render('page/about.html.twig');
    }
}