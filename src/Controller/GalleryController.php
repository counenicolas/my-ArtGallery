<?php

namespace App\Controller;




use App\Entity\Comment;
use App\Entity\Paintings;
use App\Form\CommentType;
use App\Repository\CategoryRepository;
use App\Repository\PaintingsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GalleryController extends AbstractController{


    #[Route('/gallery', name: 'gallery')]
    public function gallery(PaintingsRepository $paintingsRepository, CategoryRepository $categoryRepository): Response
    {
        $categories = $categoryRepository->findAll();
        $paintings = $paintingsRepository->findAll();

        return $this->render('gallery/paintings.html.twig',
        [
            'categories' => $categories,
            'paintings' => $paintings
        ]);
    }


    /**
     * @param PaintingsRepository $repository
     * @param int $id
     * @return Response
     */
    #[Route('/detail/{id}', name: 'detail')]
    public function detail(PaintingsRepository $repository, int$id):Response
    {
        $paintings =$repository->find($id);
      return $this->render('gallery/detail.html.twig',
      [
          'paintings' => $paintings
      ]);
    }

    /**
     * @param Paintings $paintings
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */
    #[Route('/course/{slug}', name: 'comments')]
    public function comment(Paintings $paintings,EntityManagerInterface $manager, Request $request): Response
    {
        $comment= new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $comment->setUser($this->getUser())
                    ->setPaintings($paintings)
                    ->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($comment);
            $manager->flush();
        }
        return $this->renderForm('gallery/detail.html.twig',[
            'paintings' => $paintings,
            'form' => $form,
        ]);
    }




}
