<?php

namespace App\Controller;





use App\Entity\Paintings;
use App\Form\EditPaintType;
use App\Form\PaintingsType;
use App\Repository\PaintingsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin')]
    public function admin(PaintingsRepository $repository):Response
    {
        $paintings = $repository->findAll();
        return $this->render('admin/admin.html.twig',
        [
            'paintings' => $paintings
        ]);
    }

    /**
     * @param Paintings $paintings
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     */
    #[Route('/edit/{id}', name: 'edit')]
    public function editPaint(Paintings $paintings, EntityManagerInterface $manager, Request $request): Response
    {
       $form = $this->createForm(EditPaintType::class, $paintings);
       $form->handleRequest($request);
       if ($form->isSubmitted()&&$form->isValid()){
           $manager->persist($paintings);
           $manager->flush();
           $this->addFlash(
               'success',
               'Tableau correctement mis à jour'
           );
           return $this->redirectToRoute('admin');
       }
        return $this->render('admin/edit.html.twig',[
           'form' =>$form->createView()
        ]);
    }
    #[Route('/delete/{id}', name: 'delete')]
    public function delPaint(Paintings $paintings, EntityManagerInterface $manager): Response
    {
        $manager->remove($paintings);
        $manager->flush();
        $this->addFlash(
            'success',
            'Tableau supprimé'

        );
        return $this->redirectToRoute('admin');
    }
    #[Route('/add', name : 'add')]
    public function addPaint( EntityManagerInterface $manager, Request $request): Response
    {
        $paintings = new Paintings;
        $form = $this->createForm(PaintingsType::class, $paintings);
        $form->handleRequest($request);
        if ($form->isSubmitted()&&$form->isValid()){
            $manager->persist($paintings);
            $manager->flush();
            $this->addFlash(
                'success',
                'Tableau correctement ajouté'
            );
            return $this->redirectToRoute('admin');
        }
        return $this->renderForm('admin/add.html.twig',[
            'form' =>$form
        ]);

    }
}