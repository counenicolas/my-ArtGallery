<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Paintings;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Validator\Constraints\DateTime;

class PaintingsFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $categories = $manager->getRepository(Category::class)->findAll();
        for ($i = 1; $i <= 18; $i++){
            $paintings = new Paintings();
            $paintings->setTitle($faker->words($faker->numberBetween(3, 5 ), true))
                      ->setDescription($faker->paragraphs(2, true))
                      ->setCreatedAt($faker->dateTimeBetween('-12 months', 'now'))
                      ->setHeight($faker->numberBetween(10, 150))
                      ->setWidth($faker->numberBetween(10, 150))
                      ->setImage($i.'.jpg')
                      ->setIsPublished($faker->boolean(90))
                      ->setCategory($categories[$faker->numberBetween(0, count($categories) -1)]);
            $manager->persist($paintings);
        }



        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
        // TODO: Implement getDependencies() method.
    }
}
